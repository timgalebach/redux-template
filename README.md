[![CircleCI](https://circleci.com/gh/timgalebach/redux-template/tree/master.svg?style=shield)](https://circleci.com/gh/timgalebach/redux-template/tree/master)

# DEPRECATED -- use react-redux-template repository

# redux-template
A sample application implementing Redux in React with some basic project structure imposed.

## Usage
Replace "articles" with whatever datatypes make sense, and fill out the ui section. Edit ```.env``` and fill in desired values.

### Dev
From the root directory, run
```docker-compose -f docker-compose-dev.yml up```
This will run a Docker container with the app at ```http://localhost:3000```

### Testing
From the root directory, run
```docker-compose -f docker-compose-test.yml up```
