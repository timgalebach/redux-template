import React, { Component } from "react";
import { connect } from "react-redux";
import { articleOperations } from "../../state/ducks/article";

function mapDispatchToProps(dispatch) {
  return {
    getRemoteArticles: articleOperations.getRemoteArticles
  };
}

export class RemoteArticles extends Component {
  constructor() {
    super();
  }

  componentDidMount() {
    this.props.getRemoteArticles();
  }

  render() {
    return (
      <ul className="list-group list-group-flush">
        {this.props.articles.map(el => (
          <li className="list-group-item" key={el.id}>
            {el.title}
          </li>
        ))}
      </ul>
    );
  }
}

function mapStateToProps(state) {
  return {
    articles: state.articles.remoteArticles.slice(0, 5)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RemoteArticles);