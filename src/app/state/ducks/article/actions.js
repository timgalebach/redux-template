import * as types from "./types";
import { sleep } from "../../utils"

export const addArticle = (article) => ( {
  type: types.ADD_ARTICLE,
  payload: article
});

export const getRemoteArticles = () => {
  return async (dispatch) => {
    await sleep(1500);
    var resp = await fetch("https://jsonplaceholder.typicode.com/posts")
    var json = await resp.json()
    dispatch({ type: types.REMOTE_ARTICLES_LOADED, payload: json });
  };
};