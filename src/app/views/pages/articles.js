import React from "react";
import RemoteArticles from "./remoteArticles";
import ArticleList from "./articleList";
import AddArticleForm from "./addArticleForm";

const Articles = () => (
    <div className="row mt-5">
      <div className="col-md-4 offset-md-1">
      <h2>Articles</h2>
        <ArticleList />
      </div>
      <div className="col-md-4 offset-md-1">
        <h2>Add a new article</h2>
        <AddArticleForm />
      </div>
      <div className="col-md-4 offset-md-1">
        <h2>API posts</h2>
        <RemoteArticles />
      </div>
    </div>
);

export default Articles;