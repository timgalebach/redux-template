import { Home, Articles } from "../views/pages";
//import { lazyLoad } from "../views/enhancers";

const routes = [
    {
        path: "/",
        component: Home,
        exact: true,
    },
    {
        path: "/articles",
        component: Articles,
        exact: true,
    },
    /*
    {
        path: "/articles/:permalink",
        example: "/articles/apple",
        component: ArticleDetails,
        exact: true,
    },
    */
];

export default routes;
