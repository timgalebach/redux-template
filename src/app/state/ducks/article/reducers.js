import * as types from "./types";
import { createReducer } from "../../utils";

/* State shape
{
    userAdded [],
    remoteArticles []
}
*/

const initialState = {
    userAdded: [],
    remoteArticles: []
};

const articleReducer = createReducer(initialState) ({
    [ types.ADD_ARTICLE ]: (state, action) => {
        return Object.assign({}, state, { userAdded: state.userAdded.concat(action.payload) });
    },
    [ types.REMOTE_ARTICLES_LOADED ]: (state, action) => {
         return Object.assign({}, state, { remoteArticles: state.remoteArticles.concat(action.payload)});
    },
});

export default articleReducer;