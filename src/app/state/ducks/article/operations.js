import { addArticle, getRemoteArticles } from "./actions";

export {
    addArticle,
    getRemoteArticles
};