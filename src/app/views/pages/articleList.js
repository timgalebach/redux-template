import React from "react";
import { connect } from "react-redux";

//lens for the state
const mapStateToProps = state => {
  return { articles: state.article.userAdded };
};

const ArticleList = ({ articles }) => (
  <ul className="list-group list-group-flush">
    {articles.map(el => (
      <li className="list-group-item" key={el.id}>
        {el.title}
      </li>
    ))}
  </ul>
);

export default connect(mapStateToProps)(ArticleList);