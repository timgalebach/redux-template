import { expect } from "chai";
import reducer from "../src/app/state/ducks/article/reducers";
import * as types from "../src/app/state/ducks/article/types";

describe( "article reducer", function( ) { 
    describe( "add article", function( ) {
        const title1 = "Title 1";
        const action = {
            type: types.ADD_ARTICLE,
            payload: title1
        };

        context( "empty article list", function( ) {
            const initialState = {
                userAdded: [],
                remoteArticles: []
            };
            const result = reducer( initialState, action );

            it( "should add the article to the list", function( ) {
                expect( result.userAdded.length ).to.equal( 1 );
                expect( result.userAdded[ 0 ] ).to.equal( title1 );
            } );
        });
    });
});